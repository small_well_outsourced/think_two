<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/9/11
 * Time: 15:56
 */
return [
    'imagePath' => './uploads/image/', //图片目录
    'reportPath' => './uploads/report/',//报告目录
    //工单状态 0 未接受 1 已接受 2 已取消 3 进行中 4 完成检测
    'order_status'=>[
        '0'=>'未接受',
        '1'=>'已接受',
        '2'=>'已取消',
        '3'=>'进行中',
        '4'=>'完成检测',
    ],
    //检测结果 0初始 1合格 2 不合格
    'detection_status'=>[
        '0'=>'未检测',
        '1'=>'合格',
        '2'=>'不合格',
    ],
    //仪器类型 1交流 2 直流
    'instrument_type'=>[
        '1'=>'交流',
        '2'=>'直流',
    ],
    //仪器状态 1正常 2 维修
    'instrument_status'=>[
        '1'=>'正常',
        '2'=>'维修',
    ],

    // 管理员类型
    'user_type' => [
        '1' => '超级管理员',
        '2' => '普通管理员',
        '3' => '普通用户'
    ],

    // proto 类型转换
    'proto_type' => [
        1 => 'oZeroSequenceItems',
        2 => 'oOverCurrentItems',
        3 => '',
        4 => 'ostateItem',
        5 => 'ogeneral',
        6 => ''
    ],
    // 数字转字母
    'proto_num_to_str' => [
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
    ],
    // 数字转罗马
    'proto_num_luoma' => [
        1 => 'I',
        2 => 'II',
        3 => 'III',
        4 => 'IV',
        5 => 'V',
    ]
];