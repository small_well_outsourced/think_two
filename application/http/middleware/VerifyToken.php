<?php

namespace app\http\middleware;

use app\common\JwtToken;

class VerifyToken
{
    /**
     * 检测 token 是否有效
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 11:20 AM
     * @param $request
     * @param \Closure $next
     * @return mixed|void
     * @throws \app\common\exception\AppException
     */
    public function handle($request, \Closure $next)
    {
        $token = $request->param('tokenId');
        $validationToken = JwtToken::validationToken($token);

        if (!$validationToken) {
            return app_fail(9986);
        }

        return $next($request);
    }
}
