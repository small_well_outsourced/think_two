<?php
namespace app\common\exception;

use Exception;

class AppException extends Exception
{
    protected $vars = [];

    public function __construct($code, $message, $vars = [])
    {
        $this->code    = $code;
        $this->message = $message;
        $this->vars    = $vars;
    }

    public function getVars()
    {
        return $this->vars;
    }
}