<?php
namespace app\common\exception;

use Exception;
use think\exception\Handle;

class BaseException extends Handle
{
    /**
     * 异常处理
     * User: zhouyao
     * Date: 2018/11/22
     * Time: 9:40 PM
     * @param Exception $e
     * @return \think\Response|\think\response\Json
     */
    public function render(Exception $e)
    {
        if ($e instanceof AppException) {
            return app_response($e->getCode(), $e->getMessage(), $e->getVars());
        }

        // 其他错误交给系统处理
        return parent::render($e);
    }

}