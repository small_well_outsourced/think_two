<?php
use app\common\exception\AppException;
/**
 * 公共方法都放在此处
 */
if (!function_exists('app_response')) {

    function app_response($code = 200, $data = [], $message = '', $vars = []) {

        if (!is_object($data)) {
            if (is_string($data)) {
                $message = $data;
            } elseif (empty($message)) {
                $message = config(sprintf('sys_msgs._MsgList.%s', $code));
            }

            // 替换message中可能存在的变量
            if (!empty($vars)) {
                array_unshift($vars, $message);
                $message = call_user_func_array('sprintf', $vars);
            }
        }
        if ($code !== 200) {
            $message = config(sprintf('sys_msgs._MsgList.%s', $code));
        }
        $success = 0;
        if($code == 9986)
        {
            $success = 2;
        }elseif ($code == 200)
        {
            $success = 1;
        }
        return json([
            'code' => $code,
            'msg' => $message,
            'success' => $success,
            'data' => is_null($data) || is_bool($data) || empty($data)? [] : $data
        ]);
    }
}

if (!function_exists('app_fail')) {
    /**
     *
     * User: zhouyao
     * Date: 2018/8/26
     * Time: 下午4:05
     * @param int $code
     * @param string $message
     * @param array $vars
     * @throws AppException
     */
    function app_fail($code = 9997, $message = '', $vars = []) {
        throw new AppException($code, $message, $vars);
    }
}

if (!function_exists('app_sms')) {

    /**
     * 发送短信
     * User: zhouyao
     * Date: 2018/9/14
     * Time: 下午1:57
     * @param $phone    integer 用户手机号
     * @param $code     integer 随机验证码
     * @return mixed
     * @throws AppException
     */
    function app_sms($phone, $code) {
        if (empty($phone) || empty($code)) {
            app_fail(9992);
        }

        $to = [
            'phoneNumber' => $phone,
            'contentVar' => ['code' => $code ] //模板里面的变量
        ];
        $result = \app\common\BaiduSms::sms($to);
        return $result;
    }
}

if (!function_exists('xml_parser')) {
    function xml_parser($str) {
        $xml_parser = xml_parser_create();
        if(!xml_parse($xml_parser,$str,true)){
            xml_parser_free($xml_parser);
            return false;
        }
        return true;
    }
}

if (!function_exists('user_role')) {
    /**
     * 查看是否为超级管理员
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:51 PM
     * @param null $user_id
     * @return array|null|PDOStatement|string|\think\Model
     * @throws AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function user_role($user_id = null) {
        if (is_null($user_id)) {
            app_fail(9997);
        }

        $user = db('user')->where(['id' => $user_id])->find();

        if (empty($user)) {
            return false;
        }

        if ($user['type'] != 1) {
            return false;
        }

        return true;
    }
}

if (!function_exists('user_type')) {
    /**
     * 查看是否为超级管理员
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:51 PM
     * @param null $user_id
     * @return array|null|PDOStatement|string|\think\Model
     * @throws AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function user_type($user_id = null) {
        if (is_null($user_id)) {
            app_fail(9997);
        }

        $user = db('user')->where(['id' => $user_id])->find();

        if (empty($user)) {
            return 0;
        }
        return $user['type'];
    }
}

if (!function_exists('tree')) {
    /**
     * 递归遍历数组转二维
     * User: zhouyao
     * Date: 2018/12/15
     * Time: 11:25 AM
     * @param $array
     * @param int $type
     * @return array
     */
    function tree($array, $type = 0) {
        $arr = [];
        foreach ($array as $key => $value) {
            if (isset($value['type']) && $value['type'] == $type) {
                $arr = array_merge($arr, tree($value['oitems'], 0));
            } else {
                $arr[] = $value;
            }
        }

        return $arr;
    }
}

