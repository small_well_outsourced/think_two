<?php
namespace app\common\model;

use think\Model;

class MachineModel extends Model
{
    protected $table = 'machine';
    public function  modules()
    {
        return $this->hasMany('ModuleModel', 'machine_code', 'machine_code');
    }
}