<?php
namespace app\common\model;

use think\Model;

class OrderModel extends Model
{
    protected $table = 'order';
    public function user()
    {
        return $this->belongsTo('UserModel', 'dispatch_user_id', 'id');
    }
    public function station()
    {
        return $this->belongsTo('StationModel', 'station_id', 'id');
    }
    public function protect()
    {
        return $this->belongsTo('ProtectModel', 'res_code', 'res_code');
    }
    public function template()
    {
        return $this->belongsTo('TemplateModel', 'template_id', 'id');
    }
}