<?php
namespace app\common\model;

use think\Model;

class DeviceModel extends Model
{
    protected $table = 'device_data';
}