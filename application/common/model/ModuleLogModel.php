<?php
namespace app\common\model;

use think\Model;

class ModuleLogModel extends Model
{
    protected $table = 'module_log';
}