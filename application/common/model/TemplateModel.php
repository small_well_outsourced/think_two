<?php
namespace app\common\model;

use think\Model;

class TemplateModel extends Model
{
    protected $table = 'template';
    public function  imgs()
    {
        return $this->hasMany('ImageModel', 'template_id', 'id');
    }
}