<?php
namespace app\common\model;

use think\Model;

class StationModel extends Model
{
    protected $table = 'station';
}