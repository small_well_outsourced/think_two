<?php
namespace app\admin\controller;

use app\admin\service\ModuleService;
use think\Controller;

class Module extends Controller
{
    private $moduleService;

    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
        parent::__construct();
    }

    /**
     * 显示模块信息列表
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 12:52 PM
     * @return mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }
        $result = $this->moduleService->index($input);
        return app_response(200, $result);
    }



    /**
     * 删除模块信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:43 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }
        $result = $this->moduleService->delete($input);
        return app_response(200, $result);
    }
}