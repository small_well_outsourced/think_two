<?php
namespace app\admin\controller;

use app\admin\service\OrderService;
use think\Controller;

class Order extends Controller
{
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     *
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 6:20 PM
     * @return mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->index($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 创建工单
     * @return \think\response\Json
     */
    public function create()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->create($input);

        return app_response(200, $result);
    }

    /**
     * zws
     * 查看当前工单
     * @return \think\response\Json
     */
    public function edit()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->edit($input);

        return app_response(200, $result);
    }

    /**
     * 修改当前工单
     * @return \think\response\Json
     */
    public function update()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->update($input);

        return app_response(200, $result);
    }

    /**
     * zws
     * 删除工单
     * @return \think\response\Json
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->delete($input);

        return app_response(200, $result);
    }
}