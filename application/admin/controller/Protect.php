<?php
namespace app\admin\controller;

use app\admin\service\ProtectService;
use think\Controller;

class Protect extends Controller
{
    private $service;

    public function __construct(ProtectService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     * 查看保护列表
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 6:20 PM
     * @return mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->index($input);


        return app_response(200, $result);
    }

    /**
     * 查看指定保护下对应的模板信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 3:01 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function template()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $result = $this->service->template($input);

        return app_response(200, $result);
    }

    /**
     * 删除保护下面的测试模板
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 3:13 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function deleteTemplate()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->deleteTemplate($input);
        return app_response(200, $result);
    }

    /**
     * 复制模板到其它保护下面
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 4:26 PM
     * @return mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function updateTemplate()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }

        $result = $this->service->updateTemplate($input);
        return app_response(200, $result);
    }

    /**
     * 获取所有变电站信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 3:21 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function stationList()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->stationList($input);
        return app_response(200, $result);
    }

    /**
     * 更改保护的变电站信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 3:25 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function updateStation()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->updateStation($input);
        return app_response(200, $result);
    }

    /**
     * 删除保护信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 9:57 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->delete($input);
        return app_response(200, $result);
    }

    /**
     * 显示所有保护列表
     * User: zhouyao
     * Date: 2018/12/7
     * Time: 3:30 PM
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function protectAll()
    {
        $input  = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->protectAll($input);
        return app_response(200, $result);

    }

    /**
     * 获取当前用户能查看的保护
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 11:18 AM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function protectList()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->protectList($input);
        return app_response(200, $result);
    }

    /**
     * 获取指定变电站下的保护
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 5:47 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function stationOne()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->stationOne($input);
        return app_response(200, $result);
    }

    /**
     * 获取保护下的模板
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 5:57 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function protectTemp()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->service->protectTemp($input);
        return app_response(200, $result);
    }

}