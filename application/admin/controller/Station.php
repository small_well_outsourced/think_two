<?php
namespace app\admin\controller;

use app\admin\service\StationService;
use think\Controller;

class Station extends Controller
{
    private $stationService;

    public function __construct(StationService $stationService)
    {
        $this->stationService = $stationService;
        parent::__construct();
    }

    /**
     * 显示变电站信息列表
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:09 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->stationService->index($input);
        return app_response(200, $result);
    }

    /**
     * 根据变电站查找对应的保护
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:33 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function protect()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->stationService->protect($input);
        return app_response(200, $result);
    }

    /**
     * 创建变电站信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:36 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function create()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['user_id'] = $this->request->param('guid');
        $input['create_time'] = $input['update_time'] = time();
        $result = $this->stationService->create($input);
        return app_response(200, $result);
    }

    /**
     * 删除变电站信息
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:43 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->stationService->delete($input);
        return app_response(200, $result);
    }

    /**
     * 获取所有变电站
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 5:34 PM
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function stationAll()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->stationService->stationAll($input);
        return app_response(200, $result);
    }
}