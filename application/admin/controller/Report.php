<?php
namespace app\admin\controller;

use app\admin\service\ReportService;
use think\Controller;

class Report extends Controller
{
    private $reportService;

    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
        parent::__construct();
    }

    /**
     * zws
     * 删除报告
     * @return \think\response\Json
     *
     */
    public function del_report()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->reportService->del_report($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 根据res_code 或machine_code 获取报告信息
     * @return \think\response\Json
     */
    public function report_info()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->reportService->report_info($input);
        return app_response(200, $result);
    }
}