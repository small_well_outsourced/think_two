<?php
namespace app\admin\controller;

use app\admin\service\MachineService;
use think\Controller;

class Machine extends Controller
{
    private $machineService;

    public function __construct(MachineService $machineService)
    {
        $this->machineService = $machineService;
        parent::__construct();
    }

    /**
     * 显示测试仪列表
     * User: zhouyao
     * Date: 2018/12/4
     * Time: 10:53 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->machineService->index($input);
        return app_response(200, $result);
    }

    /**
     * 删除测试仪器
     * User: zhouyao
     * Date: 2018/12/9
     * Time: 7:31 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->machineService->delete($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取当前用户能查看的测试仪
     * @return \think\response\Json
     */
    public function machineList()
    {

        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->machineService->machineList($input);
        return app_response(200, $result);
    }

    /**
     * 获取模块
     * @return \think\response\Json
     */
    public function get_module()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $result = $this->machineService->get_module($input);
        return app_response(200, $result);
    }
}