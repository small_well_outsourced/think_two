<?php
namespace app\admin\controller;

use app\admin\service\AdminService;
use think\Controller;

class Admin extends Controller
{
    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        
        $this->adminService = $adminService;
        parent::__construct();
        
    }

    /**
     * 创建用户
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 2:14 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function create()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }
        $result = $this->adminService->create($input);
        return app_response(200, $result);
    }

    /**
     * 显示管理员列表
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 2:22 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $result = $this->adminService->lists($input);
        return app_response(200, $result);
    }

    /**
     * 查看指定管理员的信息
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:44 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }
        $result = $this->adminService->edit($input);
        return app_response(200, $result);
    }

    /**
     * 更新管理员信息
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:55 PM
     * @return mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function update()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        
        $result = $this->adminService->update($input);
        return app_response(200, $result ? '操作成功' : '操作失败');
    }

    /**
     * 更改普通管理时员查看测试仪器的权限
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 4:35 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function updateRole()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }
        $result = $this->adminService->updateRole($input);
        return app_response(200, $result);
    }

    /**
     * 显示所有的测试仪器
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:48 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function showMachine()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }

        $result = $this->adminService->showMachine($input);
        return app_response(200, $result);
    }

    /**
     * 删除管理员信息
     * User: zhouyao
     * Date: 2018/12/1
     * Time: 5:58 PM
     * @return  mixed
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }

        $result = $this->adminService->delete($input);
        return app_response(200, $result);
    }

    /**
     * 后台管理员登录
     * User: zhouyao
     * Date: 2018/12/5
     * Time: 8:53 PM
     * @return mixed|\think\App
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function login()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $result = $this->adminService->login($input);
        return app_response(200, $result);
    }

    /**
     * 获取用户权限信息
     * User: zhouyao
     * Date: 2018/12/5
     * Time: 9:06 PM
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getInfo()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $result = $this->adminService->getInfo($input);
        return app_response(200, $result);
    }

    public function getDuties()
    {
        return app_response(200, [
            ['id' => 1, 'role' => '超级管理员'],
            ['id' => 2, 'role' => '普通编辑'],
            ['id' => 3, 'role' => '普通会员'],
        ]);
    }

    public function total()
    {
        $result = $this->adminService->total();
        return app_response(200, $result);
    }

    public function myProtect()
    {
        $input = $this->request->param();
        $input = json_decode($input['param'], true);
        $input['guid'] = $this->request->param('guid');
        $user = user_role($input['guid']);
        if ($user == false) {
            return app_fail(1000);
        }

        $result = $this->adminService->showProtect($input);
        return app_response(200, $result);
    }
}