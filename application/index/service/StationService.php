<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/9/8
 * Time: 11:02
 * 检测人员管理
 */
namespace app\index\service;

use app\common\model\StationModel;
use app\common\model\UserModel;
use app\common\model\ProtectModel;
use think\Exception;

class StationService
{
    protected static $stationModel;
    /**
     * UserService constructor.
     * @param StationModel $stationModel
     * @author zws
     */
    public function __construct(StationModel $stationModel)
    {
        self::$stationModel = $stationModel;
    }

    /**
     * zws
     * 上传变电站
     * @param $request
     * @return array
     */
    public function upload_station($request)
    {
        if(empty($request->station_name)){
            app_fail(8999);
        }
        if(empty($request->detail)){
            app_fail(8998);
        }
        $data = array(
            'station_name'=>$request->station_name,
            'detail'=>$request->detail,
            'user_id'=>$request->tokenId,
            'remark'=>$request->remark,
            'create_time'=>time(),
            'update_time'=>time(),
        );
        $station = self::$stationModel;
        try{
            $station->save($data);
            return ['station_id'=>$station->id];
        }catch (Exception $e){
            app_fail(9899);//编辑失败
        }
    }

    /**
     * zws
     * 获取变电站列表
     * @param $request
     * @return array
     */
    public function station_list($request)
    {
        if(empty($request->page))
        {
            $request->page = 1;
        }
        if(empty($request->limit))
        {
            $request->limit = 20;
        }
        $count = self::$stationModel->count();
        if ($count < 1) {
            return ['stations' => [], 'count' => $count];
        }
        if($request->page > 0)
        {
            $result = self::$stationModel->page($request->page, $request->limit)
                ->order('create_time', 'desc')
                ->select();
        }else
        {
            $result = self::$stationModel->order('create_time', 'desc')
                ->select();
        }
        $type = $request->role;
        if(empty($type))
        {
            $userModel = new userModel();
            $user_data = $userModel ->where('id', '=', $request->tokenId)->find();
            $type = $user_data->type;
        }
        if($result)
        {
            foreach ($result as $k=>$v)
            {
                $result[$k]->station_limit = 0;
                if($type == 1)
                {
                    $result[$k]->station_limit = 1;
                }else
                {
                    if($request->tokenId == $v['user_id'])
                    {
                        $result[$k]->station_limit = 1;
                    }
                }
            }
        }
        return ['stations' => $result, 'count' => $count];
    }

    /**
     * zws
     * 删除变电站信息
     * @param $request
     * @return array
     */
    public function del_station($request)
    {
        if(empty($request->station_id))
        {
            app_fail(8997);
        }
        $station_data = self::$stationModel->where('id', '=', $request->station_id)->find();
        if(empty($station_data))
        {
            app_fail(8996);
        }
        if(($request->role != 2 || $request->tokenId != $station_data->user_id) && $request->role
            != 1)
        {
            app_fail(9985);
        }
        try{
            $protectModel = new ProtectModel();
            $protectModel->save(['station_id'=>''], ['station_id'=>$request->station_id]);
            self::$stationModel->where(['id'=>$request->station_id])->delete();
            return true;
        }catch (Exception $e){
            app_fail(9899, $e->getMessage());//编辑失败
        }
    }
}