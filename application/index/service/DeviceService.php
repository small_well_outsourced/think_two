<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/9/8
 * Time: 11:02
 * 检测人员管理
 */
namespace app\index\service;

use app\common\model\DeviceModel;
use think\Exception;

class DeviceService
{
    protected static $deviceModel;
    /**
     * UserService constructor.
     * @param DeviceModel $deviceModel
     * @author zws
     */
    public function __construct(DeviceModel $deviceModel)
    {
        self::$deviceModel = $deviceModel;
    }

    /**
     * zws
     * 上传设备监控数据
     * @param $request
     * @return array
     */
    public function upload_device($request)
    {
        if(empty($request->machine_code)){
            app_fail(8299);
        }
        if(empty($request->links))
        {
            app_fail(8199);
        }
        $add_data = [];
        $links = is_array($request->links) ? $request->links : json_decode($request->links, true);
        foreach ($links as $k=>$v)
        {
            if(!$v['name'])
            {
                app_fail(8198);
            }
            if(!isset($v['amplitude_a']))
            {
                app_fail(8197);
            }
            if(!isset($v['amplitude_b']))
            {
                app_fail(8196);
            }
            if(!isset($v['phase_a']))
            {
                app_fail(8195);
            }
            if(!isset($v['phase_b']))
            {
                app_fail(8194);
            }
            if(!isset($v['freq_a']))
            {
                app_fail(8193);
            }
            if(!isset($v['freq_b']))
            {
                app_fail(8192);
            }
            $add_info = [
                'name' => $v['name'],
                'amplitude_a' => $v['amplitude_a'],
                'amplitude_b' => $v['amplitude_b'],
                'phase_a' => $v['phase_a'],
                'phase_b' => $v['phase_b'],
                'freq_a' => $v['freq_a'],
                'freq_b' => $v['freq_b'],
                'machine_code' => $request->machine_code,
                'create_time' => time(),
                'update_time' => time(),
            ];
            $add_data[] = $add_info;
        }

        try{
            $result = self::$deviceModel->insertAll($add_data);
            return $result;
        }catch (Exception $e){
            app_fail(9899, $e->getMessage());//编辑失败
        }
    }

    /**
     * zws
     * 获取设备监控数据列表
     * @param $request
     * @return array
     */
    public function device_list($request)
    {
        if(empty($request->page))
        {
            $request->page = 1;
        }
        if(empty($request->limit))
        {
            $request->limit = 20;
        }
        if(empty($request->machine_code)){
            app_fail(8299);
        }
        $count = self::$deviceModel->where(['machine_code'=>$request->machine_code])->count();
        if ($count < 1) {
            return ['links' => [], 'count' => $count, 'machine_code'=>$request->machine_code];
        }
        if($request->page > 0)
        {
            $result = self::$deviceModel->where(['machine_code'=>$request->machine_code])
                ->page($request->page, $request->limit)
                ->order('create_time', 'desc')
                ->select();
        }else
        {
            $result = self::$deviceModel->where(['machine_code'=>$request->machine_code])
                ->order('create_time', 'desc')
                ->select();
        }
        return ['links' => $result, 'count' => $count, 'machine_code'=>$request->machine_code];
    }

    /**
     *
     * User: zhouyao
     * Date: 2018/12/16
     * Time: 1:29 PM
     * @param $request
     * @return array
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function devicePage($request)
    {
        if(empty($request->machine_code)){
            app_fail(8299);
        }
        $count = self::$deviceModel->where(['machine_code'=>$request->machine_code])->count();
        if ($count < 1) {
            return ['links' => [], 'count' => $count, 'machine_code'=>$request->machine_code];
        }

        $result = self::$deviceModel->where(['machine_code'=>$request->machine_code])
            ->order('create_time', 'desc')
            ->paginate(20, false, [
                'query' => ['machine_code'=>$request->machine_code]
            ]);

        if ($result->isEmpty()) {
            return ['data' => [], 'page' => []];
        }

        foreach ($result as $k=>$v)
        {
            $result[$k]['date'] = $v['create_time'] ? date('Y-m-d H:i:s', $v['create_time']) : '--';
        }

        $page = $result->render();

        return ['data' => $result, 'page' => $page];
    }
}