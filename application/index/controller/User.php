<?php
namespace app\index\controller;

use app\index\service\UserService;
use think\Controller;
use app\common\JwtToken;

class User extends Controller
{
    private $userService;
    private $user_id;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct();
        $token = $this->request->param('tokenId');
        $this->user_id = JwtToken::getTokenUid($token);
    }

    /**
     * 创建用户 & 管理员
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 5:45 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function create()
    {
        $input = $this->request->param();
        $input['tokenId'] = $this->user_id;
        $result = $this->userService->create($input);
        return app_response(200, $result);
    }

    /**
     * 管理员登录
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 6:03 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function login()
    {
        $result = $this->userService->login($this->request->param());
        return app_response(200, $result);
    }

    /**
     * 短信验证码发送
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 8:35 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function sendCode()
    {
         $result = $this->userService->sendCode($this->request->param());
        return app_response(200, $result);
    }

    /**
     * 更新用户密码
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 8:43 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function updatePwd()
    {
        $input            = $this->request->param();
        $input['tokenId'] = $this->user_id;
        $result = $this->userService->updatePwd($input);
        return app_response(200, $result);
    }

    /**
     * 修改头像信息
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 9:01 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function updateImg()
    {
        $input = $this->request->param();
        $input['tokenId'] = $this->user_id;
        $input['image'] = request()->file('image');
        $result = $this->userService->updateImg($input);
        return app_response(200, $result);
    }

    public function userInfo()
    {
        $input = $this->request->param();
        $input['tokenId'] = $this->user_id;
        $result = $this->userService->userInfo($input);
        return app_response(200, $result);
    }
}