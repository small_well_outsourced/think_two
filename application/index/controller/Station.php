<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\StationService;
use think\Request;
use app\common\JwtToken;


class Station extends Controller
{
    private $stationService;

    /**
     * 依赖注入 Service
     *
     * @param StationService $stationService
     * @author zws
     */
    public function __construct(StationService $stationService)
    {
        $this->stationService = $stationService;
        parent::__construct();
    }

    /**
     * zws
     * 上传变电站
     * @param Request $request
     * @return \think\response\Json
     */
    public function upload_station(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $result = $this->stationService->upload_station($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取变电站列表
     * @param Request $request
     * @return \think\response\Json
     */
    public function station_list(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->stationService->station_list($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 删除变电站信息
     * @param Request $request
     * @return \think\response\Json
     */
    public function del_station(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->stationService->del_station($request);
        return app_response(200, $result, '删除变电站信息成功');
    }
}
