<?php
namespace app\index\controller;

use think\Controller;
use app\index\service\ReportService;

class Json extends Controller
{
    private $reportService;
    /**
     * 依赖注入 Service
     *
     * @param ReportService $reportService
     * @author zws
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
        parent::__construct();
    }
    public function test()
    {
         $json = file_get_contents(__DIR__ . '/../../../proto.json');
         $arr = json_decode($json, true);

        $proto_type = config('param_config.proto_type');

        $result = [];
        if ($arr['type'] > 0) {
            $result = [
                'type'       => $arr['type'],
                'ncheck'     => $arr['ncheck'],
                'name'       => $arr['ncheck'],
                'outputtype' => $arr['ncheck'],
                'show'       => $arr[$proto_type[$arr['type']]]
            ];
        } else {
            $data = tree($arr['oitems']);
            // 处理对应的 type 信息
            foreach ($data as $key => $val) {

                $result[] = [
                    'type'       => $val['type'],
                    'ncheck'     => $val['ncheck'],
                    'name'       => $val['ncheck'],
                    'outputtype' => $val['ncheck'],
                    'content'    => $val[$proto_type[$val['type']]]
                ];
            }
        }

//         echo json_encode($result);
        return $this->fetch();

        $arr = [
            'project_items' => [
                'type' => 0,
                'o_items' => [
                    [
                        'type' => 1,
                        'one' => ['name' => 'one', 'age' => 1],
                        'two' => ['name' => 'two', 'age' => 2],
                    ],
                    [
                        'type' => 0,
                        'o_items' => [
                            [
                                'type' => 11,
                                'one' => ['name' => 'one', 'age' => 11],
                                'two' => ['name' => 'two', 'age' => 22],
                            ]
                        ]
                    ],
                    [
                        'type' => 0,
                        'o_items' => [
                            [
                                'type' => 0,
                                'o_items' => [
                                    [
                                        'type' => 0,
                                        'o_items' => [
                                            [
                                                'type' => 11,
                                                'one' => ['name' => 'one', 'age' => 11],
                                                'two' => ['name' => 'two', 'age' => 22],
                                            ]
                                        ]
                                    ]
                                ],

                            ]
                        ]
                    ]
                ]
            ]
        ];
        $result = tree($arr);
        print_r($result);
        die;


//        $result = tree($arr['project_items']['o_items']);
//        print_r($result);die;
//        $tmp = [];
//        foreach ($result as $k => $v) {
//            if (isset($v['type']) && $v['type'] == 1) {
//                $tmp[$k]['one'] = $v['one'];
//            } else {
//                $tmp[$k]['two'] = $v['two'];
//            }
//        }
    }

    /**
     * 报告详情展示
     * User: zhouyao
     * Date: 2018/12/15
     * Time: 4:42 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function report_info()
    {
        $report_id = $this->request->param('id');
        $result = $this->reportService->report_info($report_id);
        $proto_num_to_str = config('param_config.proto_num_to_str');
        return $this->fetch('test', [
            'detail' => $result['data'],
//            'proto_one' => $result['proto_json'][0]['content'],
//            'proto_two' => $result['proto_json'][1]['content'],
            'proto' => $result['proto_json'],
            'switch_type' => [
                'name' => ['开入A', '开入B', '开入C', '开入D'],
                'biao_shi' => $proto_num_to_str,
                'luoma' => config('param_config.proto_num_luoma')
            ]
        ]);
//        return app_response(200, $result);
    }

}