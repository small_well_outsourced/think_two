<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\TemplateService;
use think\Request;
use app\common\JwtToken;

class Template extends Controller
{
    private $templateService;

    /**
     * 依赖注入 Service
     *
     * @param TemplateService $templateService
     * @author zws
     */
    public function __construct(TemplateService $templateService)
    {
        $this->templateService = $templateService;
        parent::__construct();
    }
    /**
     * zws
     * 获取模板列表
     * @param Request $request
     * @return \think\response\Json
     */
    public function template_list(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->templateService->template_list($request);
        return app_response(200, $result);
    }

    /**
     * 删除模板
     * zws
     * @param Request $request
     * @return \think\response\Json
     */
    public function del_template(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->templateService->del_template($request);
        return app_response(200, $result);
    }

    /**
     * 上传保护的模板
     * @return \think\response\Json
     */
    public function add_template()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $result = $this->templateService->add_template($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 上传模板图片
     * @return \think\response\Json
     */
    public function add_template_img()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['image'] = request()->file('image');

        $result = $this->templateService->add_template_img($input);
        return app_response(200, $result);
    }
}
