<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\ReportService;
use think\Request;
use app\common\JwtToken;

class Report extends Controller
{
    private $reportService;

    /**
     * 依赖注入 Service
     *
     * @param ReportService $reportService
     * @author zws
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
        parent::__construct();
    }

    public function createAll()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['image'] = empty($_FILES['images']) ? [] : $_FILES['images'];
        $result = $this->reportService->createAll($input);
        return app_response(200, $result);
    }

    /**
     * 管理员上传报告
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 9:38 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function create()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['image'] = request()->file('images');
        $result = $this->reportService->create($input);
        return app_response(200, $result);
    }

    /**
     * 报告历史记录列表
     * User: zhouyao
     * Date: 2018/11/28
     * Time: 3:30 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['role'] = user_type($input['tokenId']);
        $result = $this->reportService->index($input);
        return app_response(200, $result);
    }


    /**
     * 删除报告
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 10:21 PM
     * @param Request $request
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function del_report(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->reportService->del_report($request);
        return app_response(200, $result);
    }

    public function show()
    {
        $result = $this->reportService->show($this->request->param());
        return app_response(200, $result);
    }
}
