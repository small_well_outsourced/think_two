<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\DeviceService;
use think\Request;

class Device extends Controller
{
    private $deviceService;

    /**
     * 依赖注入 Service
     *
     * @param DeviceService $deviceService
     * @author zws
     */
    protected $user_id = '';
    public function __construct(DeviceService $deviceService)
    {
        $this->user_id = 1;
        $this->deviceService = $deviceService;
        parent::__construct();
    }

    /**
     * zws
     * 上传设备监控数据
     * @param Request $request
     * @return \think\response\Json
     */
    public function upload_device(Request $request)
    {
        $result = $this->deviceService->upload_device($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取监控数据
     * @param Request $request
     * @return \think\response\Json
     */
    public function device_list(Request $request)
    {
        $result = $this->deviceService->device_list($request);
        return app_response(200, $result);
    }

    /**
     *
     * User: zhouyao
     * Date: 2018/12/16
     * Time: 1:30 PM
     * @param Request $request
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function devicePage(Request $request)
    {
        $result = $this->deviceService->devicePage($request);
        return $this->fetch('', [
            'list' => isset($result['data']) ? $result['data'] : [],
            'page' => isset($result['page']) ? $result['page'] : [],
            'empty' => "<tr><td colspan='8'>暂无数据~</td></tr>"
        ]);
    }
}
