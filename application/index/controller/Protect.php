<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\ProtectService;
use think\Request;
use app\common\JwtToken;

class Protect extends Controller
{
    private $protectService;

    /**
     * 依赖注入 Service
     *
     * @param ProtectService $protectService
     * @author zws
     */
    public function __construct(ProtectService $protectService)
    {
        $this->protectService = $protectService;
        parent::__construct();
    }

    /**
     * zws
     * 上传保护
     * @param Request $request
     * @return \think\response\Json
     */
    public function upload_protect(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $result = $this->protectService->upload_protect($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取保护列表
     * @param Request $request
     * @return \think\response\Json
     */
    public function protect_list(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->protectService->protect_list($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取保护详情
     * @param Request $request
     * @return \think\response\Json
     */
    public function protect_detail(Request $request)
    {
        $result = $this->protectService->protect_detail($request);
        return app_response(200, $result, '获取保护信息成功');
    }

    /**
     * zws
     * 根据变电站id获取保护信息
     * @param Request $request
     * @return \think\response\Json
     */
    public function station_protect(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->protectService->station_protect($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 修改保护的变电站
     * @param Request $request
     * @return \think\response\Json
     */
    public function update_protect(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->protectService->update_protect($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 删除模板、保护、测试仪
     * @return \think\response\Json
     */
    public function del_data()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['role'] = user_type($input['tokenId']);
        $result = $this->protectService->del_data($input);
        return app_response(200, $result);
    }
}
