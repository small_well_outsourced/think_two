<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\index\Controller;

use think\Controller;
use app\index\service\MachineService;
use think\Request;
use app\common\JwtToken;

class Machine extends Controller
{
    private $machineService;

    /**
     * 依赖注入 Service
     *
     * @param MachineService $machineService
     * @author zws
     */
    public function __construct(MachineService $machineService)
    {
        $this->machineService = $machineService;
        parent::__construct();
    }
    /**
     * zws
     * 获取测试仪列表
     * @param Request $request
     * @return \think\response\Json
     */
    public function machine_list(Request $request)
    {
        $request->tokenId = JwtToken::getTokenUid($request->tokenId);
        $request->role = user_type($request->tokenId);
        $result = $this->machineService->machine_list($request);
        return app_response(200, $result);
    }

    /**
     * zws
     * 获取测试仪信息
     * @param Request $request
     * @return \think\response\Json
     */
    public function machine_detail(Request $request)
    {
        $result = $this->machineService->machine_detail($request);
        return app_response(200, $result, '获取测试仪信息成功');
    }

    /**
     * zws
     * 添加测试仪（只有超管有权限）
     * @return \think\response\Json
     */
    public function add_machine()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['role'] = user_type($input['tokenId']);
        $result = $this->machineService->add_machine($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 添加模块（只有超管有权限）
     * @return \think\response\Json
     */
    public function add_module()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['role'] = user_type($input['tokenId']);
        $result = $this->machineService->add_module($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 校准测试仪器
     * @return \think\response\Json
     */
    public function calibration_machine()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtToken::getTokenUid($this->request->param('tokenId'));
        $input['role'] = user_type($input['tokenId']);
        $result = $this->machineService->calibration_machine($input);
        return app_response(200, $result);
    }

}
