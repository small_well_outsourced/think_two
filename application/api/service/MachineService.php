<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/9/8
 * Time: 11:02
 * 检测人员管理
 */
namespace app\api\service;

use app\common\model\MachineLogModel;
use app\common\model\MachineModel;
use app\common\model\ModuleLogModel;
use app\common\model\ModuleModel;
use app\common\model\OperateModel;
use think\Exception;
use think\Db;
class MachineService
{
    protected static $machineModel;
    protected static $moduleModel;
    protected static $operateModel;
    protected static $machineLogModel;
    protected static $moduleLogModel;
    /**
     * MachineService constructor.
     * @param MachineModel $machineModel
     * @param OperateModel $operateModel
     * @param ModuleModel $moduleModel
     * @param MachineLogModel $machineLogModel
     * @param ModuleLogModel $moduleLogModel
     */
    public function __construct(MachineModel $machineModel,
                                OperateModel $operateModel,
                                ModuleModel $moduleModel,
                                MachineLogModel $machineLogModel,
                                ModuleLogModel $moduleLogModel)
    {
        self::$machineModel = $machineModel;
        self::$operateModel = $operateModel;
        self::$moduleModel = $moduleModel;
        self::$machineLogModel = $machineLogModel;
        self::$moduleLogModel = $moduleLogModel;
    }


    /**
     * zws
     * 获取最新校准测试仪
     * @param $machine_codes
     * @return array
     */
    public function get_last_machine($machine_codes)
    {
        $where = ' 1=1';
        if($machine_codes)
        {
            if(is_array($machine_codes))
            {
                $machine_codes_str = "'".implode("','", $machine_codes)."'";
                $where .= " and machine_code in ($machine_codes_str)";
            }else{
                $where .= " and machine_code = $machine_codes";
            }
        }
        $sql = "select a.* from (select * from machine_log where $where order by id desc) a GROUP BY a
.machine_code";
        $data = [];
        $result = self::$machineLogModel->query($sql);
        if($result)
        {
            foreach ($result as $v)
            {
                $data[$v['machine_code']] = $v;
            }
        }
        return $data;
    }

    /**
     * zws
     *获取仪器下面的模块
     * @param $machine_codes
     * @return array
     */
    public function get_module($machine_codes)
    {
        if(empty($machine_codes))
        {
            return [];
        }
        if(is_array($machine_codes))
        {
            $where1[] = ['machine_code', 'in', $machine_codes];
        }else{
            $where1 = ['machine_code'=>$machine_codes];
        }
        $module_datas = self::$moduleModel->where($where1)->select();
        if(!$module_datas)
        {
            return [];
        }
        $module_data_arr = [];
        $module_codes = [];
        foreach ($module_datas as $v)
        {
            $module_codes[] = $v['module_code'];
        }
        $last_module_datas = $this->get_last_module($module_codes);
        foreach ($module_datas as $v)
        {
            if(isset($last_module_datas[$v['module_code']]))
            {
                $last_module_datas[$v['module_code']]['date'] = $last_module_datas[$v['module_code']]['create_time'] ?
                    date("Y-m-d H:i:s", $last_module_datas[$v['module_code']]['create_time']) : '';
                $module_data_arr[$v['machine_code']][] = $last_module_datas[$v['module_code']];
            }
        }

        return $module_data_arr;
    }

    /**
     * zws
     * 根据模块id获取最新模块信息
     * @param $module_codes
     * @return array
     */
    public function get_last_module($module_codes)
    {
        if(empty($module_codes))
        {
            return [];
        }
        $module_codes_str = "'".implode("','", $module_codes)."'";
        $where = " module_code in ($module_codes_str)";
        $sql = "select a.* from (select * from module_log where $where order by id desc) a GROUP BY a
.module_code";
        $data = [];
        $result = self::$machineLogModel->query($sql);
        if($result)
        {
            foreach ($result as $v)
            {
                $v['module_id'] = $v['module_code'];
                $data[$v['module_code']] = $v;
            }
        }
        return $data;
    }
    /**
     * 获取测试仪信息
     * @param $request
     * @return array|null|\PDOStatement|string|\think\Model
     */
    public function machine_detail($request)
    {
        if(empty($request->machine_code))
        {
            app_fail(8299);
        }
        $machine_data = self::$machineModel->where(['machine_code'=>$request->machine_code])->find();
        if(empty($machine_data))
        {
            app_fail(8298);
        }
        $last_machine_data = $this->get_last_machine($request->machine_code);
        $last_machine_arr = isset($last_machine_data[$request->machine_code]) ?
            $last_machine_data[$request->machine_code] : $machine_data;
        $machine_data->start_date = $machine_data->create_time ? date('Y-m-d H:i:s', $machine_data->create_time) : '';
        $machine_data->end_date = $last_machine_arr['create_time'] ? date('Y-m-d H:i:s',
            $last_machine_arr['create_time']) : '';
        $machine_data->machine_detail = $last_machine_arr['machine_detail'];
        if($last_machine_arr['remark'])
        {
            $machine_data->remark = $last_machine_arr['remark'];
        }
        $machine_data->machine_limit = 0;
        $machine_data->modules = [];
        return $machine_data;
    }

}