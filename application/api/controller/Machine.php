<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\api\Controller;

use think\Controller;
use app\api\service\MachineService;
use think\Request;

class Machine extends Controller
{
    private $machineService;

    /**
     * 依赖注入 Service
     *
     * @param MachineService $machineService
     * @author zws
     */
    public function __construct(MachineService $machineService)
    {
        $this->machineService = $machineService;
        parent::__construct();
    }

    /**
     * zws
     * 获取测试仪信息
     * @param Request $request
     * @return \think\response\Json
     */
    public function machine_detail(Request $request)
    {
        $result = $this->machineService->machine_detail($request);
        return app_response(200, $result, '获取测试仪信息成功');
    }
}
