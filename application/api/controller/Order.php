<?php
/**
 * Created by PhpStorm.
 * User: zws
 * Date: 2018/11/23
 * Time: 09:43
 */
namespace app\api\Controller;

use think\Controller;
use app\api\service\OrderService;
use app\common\JwtAppToken;

class Order extends Controller
{
    private $orderService;

    /**
     * 依赖注入 Service
     *
     * @param OrderService $orderService
     * @author zws
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
        parent::__construct();
    }

    /**
     * zss
     * 拉取工单
     * @return \think\response\Json
     */
    public function order_list()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtAppToken::getTokenUid($this->request->param('tokenId'));
        $result = $this->orderService->order_list($input);
        return app_response(200, $result);
    }

    /**
     * zws
     * 接手工单
     * @return \think\response\Json
     */
    public function update_order()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtAppToken::getTokenUid($this->request->param('tokenId'));
        $result = $this->orderService->update_order($input);
        return app_response(200, $result, '接受成功');
    }
}
