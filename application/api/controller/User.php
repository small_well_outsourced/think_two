<?php
namespace app\api\controller;

use app\api\service\UserService;
use think\Controller;
use app\common\JwtAppToken;
use think\facade\Log;

class User extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct();
    }

    /**
     * 创建用户 & 管理员
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 5:45 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function create()
    {
        $input = $this->request->param();
        $input['image'] = request()->file('image');
        $result = $this->userService->create($input);
        return app_response(200, $result);
    }

    /**
     * 管理员登录
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 6:03 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function login()
    {
        $result = $this->userService->login($this->request->param());
        return app_response(200, $result);
    }

    /**
     * 短信验证码发送
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 8:35 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function sendCode()
    {
         $result = $this->userService->sendCode($this->request->param());
        return app_response(200, $result);
    }

    /**
     * 更新用户密码
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 8:43 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function updatePwd()
    {
        $input = $this->request->param();
        $input['tokenId'] = JwtAppToken::getTokenUid($this->request->param('tokenId'));
        $result = $this->userService->updatePwd($input);
        return app_response(200, $result);
    }

    /**
     * 修改头像信息
     * User: zhouyao
     * Date: 2018/11/27
     * Time: 9:01 PM
     * @return \think\response\Json
     * @throws \app\common\exception\AppException
     */
    public function updateImg()
    {
//        Log::write($_FILES['image']);
        $input = $this->request->param();
        $input['tokenId'] = JwtAppToken::getTokenUid($this->request->param('tokenId'));
        $input['image'] = request()->file('image');
        $result = $this->userService->updateImg($input);
        return app_response(200, $result);
    }

    /**
     * 获取用户信息
     * @return [type] [description]
     */
    public function userInfo()
    {
        $input['tokenId'] = JwtAppToken::getTokenUid($this->request->param('tokenId'));
        $result = $this->userService->userInfo($input);
        return app_response(200, $result);        
    }


}