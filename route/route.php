<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/**
 *  后台管理组
 */
Route::get('/', function () {
    return '<h2>欢迎使用</h2>';
});
Route::group('admin', function () {
    Route::post('/total', 'Admin/total');
    Route::post('/user/create', 'Admin/create'); // 创建管理员
    Route::post('/user/index', 'Admin/index'); // 查看管理员列表
    Route::post('/user/edit', 'Admin/edit'); // 查看指定管理员信息
    Route::post('/user/updateRole', 'Admin/updateRole'); // 查看指定管理员信息
    Route::post('/user/showMachine', 'Admin/showMachine'); // 显示所有测试仪器列表
    Route::post('/user/delete', 'Admin/delete'); // 删除管理员
    Route::post('/login', 'Admin/login'); // 登录
    Route::post('/getInfo', 'Admin/getInfo'); // 用户权限信息
    Route::post('/getDuties', 'Admin/getDuties'); //用户类型
    Route::post('/user/update', 'Admin/update'); // 更新管理员信息

    /* 保护信息模块管理 */
    Route::post('protect/index', 'Protect/index'); // 查看保护列表
    Route::post('protect/template', 'Protect/template'); // 查看保护测试模板
    Route::post('protect/deleteTemplate', 'Protect/deleteTemplate'); // 删除保护测试模板
    Route::post('protect/updateStation', 'Protect/updateStation'); // 更改 & 新增保护变电站信息
    Route::post('protect/updateTemplate', 'Protect/updateTemplate'); // 复制指定保护模板到其它保护下
    Route::post('protect/delete', 'Protect/delete'); // 删除保护
    Route::post('protect/stationList', 'Protect/stationList'); // 显示所有变电站列表
    Route::post('protect/protectAll', 'Protect/protectAll'); // 显示所有保护列表
    Route::post('protect/protectList', 'Protect/protectList'); // 获取当前用户能查看的保护

    /* 变电站信息管理 */
    Route::post('station/index', 'Station/index'); // 变电站信息列表
    Route::post('station/protect', 'Station/protect'); // 变电站信息列表
    Route::post('station/create', 'Station/create'); // 变电站信息列表
    Route::post('station/delete', 'Station/delete'); // 变电站信息列表

    /* 测试仪信息管理 */
    Route::post('machine/index', 'Machine/index'); // 测试仪信息列表
    Route::post('machine/machineList', 'Machine/machineList'); // 获取当前用户能查看的测试仪
    Route::post('machine/getModule', 'Machine/get_module'); // 获取测试仪下的模块
    Route::post('machine/delete', 'Machine/delete'); // 删除测试仪器

    /* 工单信息管理 */
    Route::post('order/create', 'Order/create'); // 创建工单
    Route::post('order/edit', 'Order/edit'); // 查看指定工单
    Route::post('order/update', 'Order/update'); // 修改工单
    Route::post('order/index', 'Order/index'); // 查看工单列表
    Route::post('order/delete', 'Order/delete'); // 删除工单信息
    /** 显示所有保护 */
    Route::post('order/stationAll', 'Station/stationAll');
    Route::post('order/stationProtectOne', 'Protect/stationOne'); // 变电站下的保护
    Route::post('order/protectTemp', 'Protect/protectTemp'); // 获取保护下的模板
    /* 模块信息管理 */
    Route::post('module/index', 'Module/index'); // 查看模块列表
    Route::post('module/delete', 'Module/delete'); // 删除模块信息


    /* 报告信息管理 */
    Route::post('report/report_info', 'Report/report_info'); // 根据res_code 或machine_code 获取报告信息
    Route::post('report/delete', 'Report/del_report'); // 删除报告


})->prefix('admin/')->allowCrossDomain();

/**
 * PC 管理组
 */
Route::post('index/login', 'User/login'); // 管理员登录
Route::post('index/sendCode', 'User/sendCode'); // 发送短信验证码
Route::post('index/show', 'Report/show'); // 根据二维码获取保护、模板、报告
Route::post('index/machineDetail', 'Machine/machine_detail');//获取测试仪器信息
Route::post('index/protectDetail', 'Protect/protect_detail');//获取保护详情
Route::post('index/deviceList', 'Device/device_list');//获取设备检测数据
Route::group('index', function (){
    /**
     * 用户模块
     */
    Route::post('create', 'User/create'); // 创建用户
    Route::post('userInfo', 'User/userInfo'); // 获取用户信息

    Route::post('updatePwd', 'User/updatePwd'); // 修改用户密码
    Route::post('updateImg', 'User/updateImg'); // 修改用户头像图片

    /**
     * 报告模块
     */
    Route::post('addReportAll', 'Report/createAll'); // 批量创建报告
    Route::post('addReport', 'Report/create'); // 创建报告
    Route::post('reportList', 'Report/index'); // 报告下载列表

    Route::post('addStation', 'Station/upload_station');//添加变电站
    Route::post('stationList', 'Station/station_list');//获取变电站列表
    Route::post('delStation', 'Station/del_station');//删除变电站

    Route::post('addProtect', 'Protect/upload_protect');//添加保护
    Route::post('protectList', 'Protect/protect_list');//获取保护数据
    Route::post('stationProtect', 'Protect/station_protect');//获取变电站所有保护
    Route::post('updateProtect', 'Protect/update_protect');//修改保护的变电站
    Route::post('delData', 'Protect/del_data');//删除模板、保护、测试仪

    Route::post('addDevice', 'Device/upload_device');//上传设备检测数据

    Route::post('addMachine', 'Machine/add_machine');//获取该管理员能看到的测试仪
    Route::post('calibrationMachine', 'Machine/calibration_machine');//获取该管理员能看到的测试仪
    Route::post('machineList', 'Machine/machine_list');//获取该管理员能看到的测试仪
    Route::post('addModule', 'Machine/add_module');//添加模块

    Route::post('templateList', 'Template/template_list');//获取模板列表
    Route::post('delTemplate', 'Template/del_template');//删除模板
    Route::post('delReport', 'report/del_report');//删除报告
    Route::post('addResTemplate', 'Template/add_template');//上传保护的模板
    Route::post('addTemplateImg', 'Template/add_template_img');//上传模板图片

})->prefix('index/')->middleware('VerifyToken')->allowCrossDomain();


/**
 * Api 管理组
 */
Route::group('app', function (){

    /**
     * 用户模块
     */
    Route::post('userInfo', 'User/userInfo'); // 会员信息
    Route::post('updatePwd', 'User/updatePwd');//修改密码
    Route::post('updateImg', 'User/updateImg'); // 修改用户头像图片

    Route::post('addReportAll', 'Report/createAll'); // 批量创建报告
    Route::post('addReport', 'Report/create'); // 创建报告
    Route::post('reportList', 'Report/index'); // 注册用户历史报告下载

    Route::post('updateOrder', 'Order/update_order'); // 接受工单
    Route::post('uploadReport', 'Report/upload_report'); // 上传工单报告，工单结束
    Route::post('orderList', 'Order/order_list'); // 工单列表

})->prefix('api/')->middleware('VerifyAppToken')->allowCrossDomain();

Route::post('app/register', 'api/User/create');//用户注册
Route::post('app/login', 'api/User/login');//用户登录
Route::post('app/sendCode', 'api/User/sendCode');//发送验证码
Route::post('app/show', 'api/Report/show'); // 根据二维码获取保护、模板、报告
Route::post('app/machineDetail', 'api/Machine/machine_detail');//获取测试仪器信息

Route::get('/json', 'index/Json/test');
Route::get('/report_info', 'index/Json/report_info');
Route::get('index/devicePage', 'index/Device/devicePage');